<!-- 
0.1 - Date - not known.  Originally developed after a bad experience with the First Campus day, when the logins didn't work and the students really struggled to keep up. 
0.2 - Minor updates, just checking this will work for an Applicant Day. 

-->
# First Campus 2020 #

## Introduction ##

Welcome to the Activity for First Campus 2020, in a very short time we aim to give you a taste of game development.

If your reading this before we've done the demo, please wait.  These notes are intended to provide a point of reference to the steps required.

## Activity ##

Due to time constraints the activity we have for you is short, however, you can take this away and work on it in your own time.  I've split this into two key stages, first creating some content for the game (what we call an **asset**,) then adding your asset to the game by making modifications changes to the game code.

### Getting the Materials ###

You'll need to work on your own copy of the game, to do this you'll need to download a copy of this project.

#### Steps ####
1. Go to [bitbucket](https://bitbucket.org/pangel17/spooky/)
2. Click the **Downloads** link ![w:300](images/Bitbucket1.png)
<!--* Alternativly use this [link](https://bitbucket.org/pnangel17/spooky/downloads/). -->
3. Click **Download repository** ![w:300](images/Bitbucket2.png)
4. Save in the **Downloads** folder.
5. Unzip (right click - "Extract Here")

### Creating a Spritesheet with Piskel ###

The first step to adding a new NPC (Non Player Character) to the **Spooky** game is to create a sprite sheet (or sprite atlas).  A sprite is a 2D image or animation (more than one image) we can move/rotate on screen, these can be downloaded or created. ![](/images/Sprite.png)

A sprite sheet is an image made up of multiple other images Which may be:
* whole objects (like a film reel / or flickbook) ![](/images/SpriteSheet.png)
* component parts of something animated by the game ![](/images/SpriteSheet2.png)

Usually a sprite sheet will contain more than one animation for a single character, in the image below there are animations for running, jumping, swinging, fighting etc. ![](https://gamesrevisiteddotcom.files.wordpress.com/2014/05/genesis32xscd-aladdin-ii-pirate-aladdin-1.gif?w=274&h=409).

We're going to create one to replace this sprite sheet:

![](../../Assets/Images/Mummy-sprite-Sheet.png)

Later we'll create a file to provide the other information required (what we call meta-data, that is data about data).  This will include the name of the thing being animated, the size of the image, the animation sequences present (walk, run etc.) and which images belong to which sequence.

#### Steps ####
1. Open [Piskel](https://www.piskelapp.com/)
2. Create your sprite!
  1. Use [this](../UserGuides/Piskel_Sprite_Editor.pdf) guide.

<!-- TODO re-write this guide -->

### Creating the Sprite Meta Data ###

In addtion to our sprite sheet (containing the frames of our animation) we'll also need a file containing the metadata. In addtion to our sprite sheet (containing the frames of our animation) we'll also need a file containing the metadata.  This will include the name of the thing being animated, the size of the image, the animation sequences present (walk, run etc.) and which images belong to which sequence.  So we'll use a tool called the Spritesheet Editor. We could create this file ourselves but this would take a long time and prone to error, so we'll use a tool called the Spritesheet Editor.

#### Steps ####

1. Open the SpriteSheet Editor (```spritesheet_editor.html```) this must be the one you downloaded.
2. Create your metadata
  * Use [this](../UserGuides/Spritesheet_editor_user_guide.pdf) guide.

<!-- TODO re-write this guide -->

### Adding YOUR Sprite to the Game ###
#### Steps ####
1. Make sure Firefox calls the script containing the sprite meta data.
  * Open ```game.html```
  * Find this line ```<script src='Animations/mummy.anim.js'></script>```
  * Add a new line for your script e.g. ```<script src='Animations/ball.anim.js'></script>```
2. Add your new Sprite to the game.
  * Open ```Engine/levels.js```
  * The line ```  dynamicElements : [``` defines the start of an array of dynamic elements (NPCs and platforms).
  * Each looks like
  ```js
  { factory : Game.Model.Creature, // Type determines actual character / behaviour
      model : {
        anim_id : 'mummy', // id determines animation sequence
        pos : { x : 64, y : 192}, // position
        size : { width : 64, height : 64 }, // size
        colliderType : 'mummy', // collision info.
        collisionFilter : Game.Model.CollisionFilter.NPC,
        initState : 'Alive'
      } },
  ```
  * Change this to follow the metadata of your NPC sprite, e.g.
  ```js
  { factory : Game.Model.Creature,
      model : {
        anim_id : 'ball', // your object identifier
        pos : { x : 64, y : 192},
        size : { width : 32, height : 32 }, //your size
        colliderType : 'ball', // as above
        collisionFilter : Game.Model.CollisionFilter.NPC,
        initState : 'Alive'
      } },
  ```
  * Save the file.
3. Open ```Game.html``` with **Firefox** (or reload the page), you should find your new sprite onscreen. 
