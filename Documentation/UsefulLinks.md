# Useful Links #

## General ##
### Useful resources, articles and discussion on all things game development ###
[GameDev.net](https://www.gamedev.net/)
[Gameasutra](http://www.gamasutra.com/)

###  Game Genres ###
[list](https://en.wikipedia.org/wiki/List_of_video_game_genres)

### Game Engines ###
[Unity3D](https://unity3d.com/)
[Unreal Engine](https://www.unrealengine.com/what-is-unreal-engine-4)
[CryEngine](https://www.cryengine.com/)

### Coding tools ###
[Visual Studio](https://www.visualstudio.com/)
[Android Studio](https://developer.android.com/develop/index.html)
[XCode](https://developer.apple.com/)

## Web-based Games ##

### General Web Development Resources ###
[W3 Schools](https://www.w3schools.com/)
[Web Audio](http://www.w3.org/TR/webaudio/)
[NodeJS](https://nodejs.org/en/)

### Game Engine ###
[Phaser](https://phaser.io/)

### Physics Engine ###
[Matter-JS](http://brm.io/matter-js/)

### Sprite Editor ###
[Piskel](http://www.piskelapp.com/)

### Audio resources ###
[Free Sound](https://freesound.org/)
