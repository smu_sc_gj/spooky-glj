<!-- Version 0.1 - Making a version of paul's guide I can use with different tasks.  This is a rough copy, I need to change the images and move away from fixed lables. 
Version 0.2 - Created a version of the user guide for the ball example.  I'm going to use this tomorrow and when time allows create a version for player. 
-->

# Spritesheet Editor - User Guide #

## Content ##
1. Opening the Spreadsheet Editor
2. Layout
3. Loading a Spritesheet
4. Configuring
5. Adding Animation States / Sequences
6. Exporting

## Opening the Spritesheet Editor ##

The editor is a one page web application developed specifically for the **Forge** engine as part of the same project as the Spooky game. 

1. Loading the Spritesheet Editor

    ![](images/loadingSpriteEditor.png)

    * To run the editor, right-click ```spritesheet_editor.html``` file.
    * Select ```Open With``` and ```Firefox```
    * **Firefox** is our browser of choice here due to the use of local files for resources. 

## Editor Layout ##

![](images/EditorLayout2.png)

## Loading a Spritesheet ##

To load a sprite there are three steps:

1. Create a new spritesheet model

    ![](images/OpenButton-New.png)

2. Select **Import** and pick a suitable image file.  **This must be stored in the Assets/Images folder.**

![](images/SpriteSheetInfo-Import.png)

3. The sprite sheet should be shown in the center
![](images/LoadedSpriteSheetOnly.png)

4. Give the spritesheet a suitable name - I've gone for *ball*

![](images/SpriteSheetName-complete.png)

### Example ###

Here's one I created earlier:

![](images/SpriteEditor-Ball-Partdone.png)

## Configuring a Spritesheet ##

The sprite editor needs information about the content.  This includes the size of the sprites and the rate at which they need to be displayed. 

1. Click **Configure** to bring up the spritesheet configuration dialog…

![](images/Configure1-BallEg.png)

This launches the **Configure** dialog. Here you can configure the width and height of each frame as well as the number of frames per second (25 fps for example).
Once done click **Accept**

![](images/AnimationProps-Ball-done.png)

The spritesheet view is now spit into individual frames (as shown below):

<!-- Example changes! -->

<!-- ![](images/BraidExample.png) -->
![](images/BallExample-AnimiationFrames.png)


<!-- In the above we're using Frost's character animation from the game [Braid](http://braid-game.com/).-->

We now have the information about where the sprites are within the sprite sheet. An obvious issue is that we didn't use all the frames.  In more complex example we might have run, walk, jump etc. for a single character all contained on a single sheet. 

## Adding Animation States / Sequences ##

Once the spritesheet has been configured, we can add individual animation states / sequences to be used in the game engine. <!-- This document will outline the animation states for an example player where the player can stand, walk, jump and fall. For a side-scrolling game we want left / right facing animations as well. The list below shows the sequences we need for our example player character. -->
As time is short, we're going to replace the NPC in the Spooky game (the 'mummy') character. This has a single animation state **Alive**.

1. To add animation sequences use the add button:
![](images/AddImagesButton-highlight.png)

Tools such as this are designed to create data that links the art assets (our spritesheet) with the main game engine.  The game engine expects each of the required states / animation sequences to be given a unique name – these are listed in red in the table below.
<!-- 
|Sequence|Sequence Identifier|
|:-:|:-:|:-:|
|Stand (facing left)|Standing_left|
|Stand (facing right)|Standing_right|
|Walk left|Walking_left|
|Walk right|Walking_left|
|Jump left|Jumping_left|
|Jump right|Jumping_right)|
|Fall left|Falling_left|
|Fall right|Falling_right|
-->
|Sequence|Sequence Identifier|
|:-:|:-:|:-:|
|Alive (moving)|**Alive**|
|Dead (still?)|?|

The **Sequence Identifiers** above are defined in the game engine for each object:
    - Need to use the right ones
    - This includes case ...

### New Animation Sequence Dialog ### 

The **New Animation Sequence** dialogs shows the following fields to complete. 

![](images/NewSequenceDialogFull.png)

**Object Identifier** will be the name of the object the animation will apply to in the game engine.  For our player example this will simply be player

![](images/NewAnimSeq-OI.png)

The **Unique Sequence Name** will be one of the Sequence Identifiers from the table shown on the previous page.  So if you’re configuring the walking left animation this will be Walking_left

![](images/NewAnimSeq-USN.png)

Depending on your spritesheet, here you enter the start frame and end frame number for the animation sequence you’re working on.  Frame numbers are shown in the main spritesheet view and start at 0.  <!-- For example, if our Walking_left sequence went from frame 0 to frame 4 we’d enter 0 in the **Start Frame** field and 4 in the **End Frame** field. --> For example if our animation starts in frame 0 and finishes in frame 7, we’d enter 0 in the **Start Frame** field and 7 in the **End Frame** field.

![](images/NewAnimSeq-Frame.png)

Depending on your spritesheet, you may want your animation to oscillate.  What does this mean?  Let’s go back to our Walking_left sequence between frames 0 and 4 (0, 1, 2, 3, 4).

If **Oscillate** is unchecked, when we play the animation we show frames in the following order: 0 1 2 3 4 and simply repeat this sequence.  So when repeated, the sequence of frames is…

0 1 2 3 4 0 1 2 3 4 0 1 2 3 4 …

If **Oscillate** is checked, we play the sequence of frames forwards (0, 1, 2, 3, 4), then backwards (3, 2, 1, 0), then forwards again (1, 2, 3, 4) and so on.  So playing the animation shows the frames in the following sequence.

0 1 2 3 4 3 2 1 0 1 2 3 4 …

![](images/NewAnimSeq-O.png)

The next checkbox is called **Flip Horizontal**.  This tells the game engine to flip our animation frames from left-to-right (or visa-versa) or leave them as they are in the spritesheet.

If your spritesheet contains animation frames for left and right sequences you can leave this unchecked.
<!--
If, like our example Braid animation, all of the animation frames face in one direction (facing right in this case), we can set this checkbox for left-facing animations.

So, for the Walking_right sequence for example we’d leave this unchecked (since our spritesheet contains the character facing right). 

However, for the Walking_left sequence we can set the start and end frames to be the same as those for the Walking_right sequence but check the **Flip Horizontal** checkbox to tell the game engine we want to show the frames facing left, not facing right, when we walk left in the game.  -->

![](images/NewAnimSeq-FH.png)

The final checkbox is called **Flip Vertical**.  This works the same as Flip Horizontal, but when checked the game engine will flip the sprite frames top-to-bottom rather than from left-to-right.

![](images/NewAnimSeq-FV.png)

Once complete, click the Create button to create the animation sequence.

![](images/NewAnimSeq-CreateButtonHighlighted.png)

Once created new animation sequences will appear in the Animation Sequences list on the right side of the webpage. 

![](images/AnimationSequences-BallComplete.png)

You can delete the sequence by clicking the Bin/Trash icon.  

# Loading and Saving #

The editor provides features to load and save animations.  This is to allow animations to be re-loaded into the editor, we'll need to **export** to the runtime (game) format.

## Saving ##

There are three stages to saving:
1.  When you’re ready to save your spritesheet model, click the disk icon at the top of the webpage.
    ![](images/TopBar-Saving-Save.png)

2. 2. You’ll be asked for the name of the file.  Here we’ll call the file <!-- player1_animation -->*ball*.  Click OK to proceed with the save, or Cancel to go back to the main editor.
![](images/SaveDialog1.png)

3. You’ll be presented with a standard save dialog.  With Save File selected, click OK.  Depending on how your browser is configured you may be asked to specify where to save the file to.  By default this is usually your Downloads folder. 

    You’ll notice the filename has been extended with ```forge.sps.json```.

    *Note:* The file that is saved is actually a text file with the extension ```.json```.  JSON stands for JavaScript Object Notation and is a convenient way of storing datasets.

![](images/PlayerWindowSaveDialog.png)


## Loading ##

Loading is a little simpler, simply click the Open button from the top menu:

![](images/TopBar-Saving-Open.png)

The **Forge** Sprite Editor expects to find the animations it will load in local folder (**Models**). 

![](images/Loading1.png)

 If you want to reload your saved spritesheet model, make sure you move your saved file into the Models folder which can also be found in the main game folder.

# Exporting # 

The process of exporting is very similar to saving, there are three steps:

1. When you’re happy that the animations are all complete and ready for use in the game, you need to export the animation model.
![](images/TopBar-Saving-Save.png)

2.  Again you’ll be asked for the name of the file.  Here we’ll call the file <!-- player1_animation-->*ball.anim* (no . at the end).   Click **OK** to proceed with the export, or **Cancel** to go back to the main editor.

    ![](images/Exporting-ball1.png)

3. You’ll be presented with a dialog asking you to save the file.  Again, depending on how your browser is configured you may be asked to specify where to save the file to.  By default this is usually your Downloads folder.

    You’ll notice the filename has been extended with .js.

    Unlike Save, Export creates a runnable JavaScript file that can be used in the game engine.

    ![](images/PlayerWindowExportDialog.png)

4.  In order to be accessible to the game engine, move the exported file into the Animations folder, which as before, is located in the main game folder.

    ![](images/MoveAnimationToFolderWindows.png)

The new spritesheet and animation data are now ready to be loaded into the game. 