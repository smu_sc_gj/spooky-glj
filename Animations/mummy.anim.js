'use strict';

// Game.Animation contains the spritesheet and sequences used to 'skin' an in-game character type (such as player).  Each 'animation' has two parts: A spritesheet and sequence definition.  Both are linked by a common identifier.  Here 'mummy' uniquely identifies the spritesheet and associated animation sequences.
Game.Animation = (function(module) {

	module.spritesheet = (module.spritesheet || {});
	module.sequence = (module.sequence || {});
  
  // Create new spritesheet (skin) 'mummy' for Game.Model.Creature
	module.spritesheet['mummy'] = Game.Graphics.SpriteSheet({

		imageURL : 'Assets/Images/Mummy-sprite-Sheet.png',
		frameWidth : 250,
		frameHeight : 250,
		framesPerSecond : 12
	});

  // Create new animation 'mummy' for Game.Model.Creature.
  // Note: Animation states are dictated by the underlying class.
  // Game.Model.Creature defines the following relevant states: ['Alive']
  
  let anim_id = 'mummy';
  
	module.sequence[anim_id] = {};
  
	module.sequence[anim_id]['Alive'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['mummy'], // Each sequence can reference separate spritesheets.  It's the anim_id key that links in-game instances to sequences.
		startFrame : 0,
		endFrame : 9,
		oscillate : true,
		flipHorizontal : false,
		flipVertical : false
	});

	return module;

})((Game.Animation || {}));