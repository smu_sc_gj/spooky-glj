Game.Animation = (function(module) {

	module.spritesheet = (module.spritesheet || {});
	module.sequence = (module.sequence || {});

	module.spritesheet['braid'] = Game.Graphics.SpriteSheet({

		imageURL : 'Assets/Images/FrostAnim01b.png',
		frameWidth : 74,
		frameHeight : 86,
		framesPerSecond : 25
	});


  let anim_id = 'player';
  
	module.sequence[anim_id] = {};
	
  module.sequence[anim_id]['Standing_right'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['braid'],
		startFrame : 20,
		endFrame : 20,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence[anim_id]['Standing_left'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['braid'],
		startFrame : 20,
		endFrame : 20,
		oscillate : false,
		flipHorizontal : true,
		flipVertical : false
	});

	module.sequence[anim_id]['Walking_right'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['braid'],
		startFrame : 0,
		endFrame : 26,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence[anim_id]['Walking_left'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['braid'],
		startFrame : 0,
		endFrame : 26,
		oscillate : false,
		flipHorizontal : true,
		flipVertical : false
	});

	module.sequence[anim_id]['Jumping_right'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['braid'],
		startFrame : 23,
		endFrame : 23,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence[anim_id]['Jumping_left'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['braid'],
		startFrame : 23,
		endFrame : 23,
		oscillate : false,
		flipHorizontal : true,
		flipVertical : false
	});

	module.sequence[anim_id]['Falling_right'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['braid'],
		startFrame : 17,
		endFrame : 17,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	module.sequence[anim_id]['Falling_left'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['braid'],
		startFrame : 17,
		endFrame : 17,
		oscillate : false,
		flipHorizontal : true,
		flipVertical : false
	});

	return module;

})((Game.Animation || {}));