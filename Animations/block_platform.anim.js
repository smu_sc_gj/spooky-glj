Game.Animation = (function(module) {

	module.spritesheet = (module.spritesheet || {});
	module.sequence = (module.sequence || {});

	module.spritesheet['block_platform'] = Game.Graphics.SpriteSheet({

		imageURL : 'Assets/Images/platform1.png',
		frameWidth : 64,
		frameHeight : 16,
		framesPerSecond : 1
	});


  let anim_id = 'block_platform';
  
	module.sequence[anim_id] = {};
  
	module.sequence[anim_id]['Moving'] = Game.Graphics.AnimationSequence({

		spriteSheet : module.spritesheet['block_platform'],
		startFrame : 0,
		endFrame : 0,
		oscillate : false,
		flipHorizontal : false,
		flipVertical : false
	});

	return module;

})((Game.Animation || {}));