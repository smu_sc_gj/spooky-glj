'use strict';


// This module contains the sprites you'll create for your level tiles.  Sprites are given a unique id number - so in these examples cobblestone has id of 1, grass has an id of 2.
// *** ADD YOUR OWN TILE SPRITES HERE - GIVE EACH TILE SPRITE A UNIQUE ID ***
Game.Sprites = (function(sprites) {
  
  sprites[1] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/cobblestone.jpg' });
  sprites[2] = Game.Graphics.Sprite({ imageURL : 'Assets/Images/grass_tile.png' });
  
  return sprites;

})(Game.Sprites || []);


// The Levels module contains the data needed to create an actual level, including the backgrounds we want to use as well as the level tile layout, player, NPC and platform setup.
Game.Levels = (function(module) {
  
  module.Level = (module.Level || []);
  
  // This example sets up a single level (with unique id 1 for 'level 1' in this case!)
  module.Level[1] = {
    
    name : 'Spooky level 1',
    
    // background provides a viewport aligned image (This remains fixed as you move around the level)
    // *** REPLACE THIS WITH YOUR OWN BACKGROUND IMAGE HERE ***
    background : {
      
      image : Game.Graphics.Sprite( {imageURL:'Assets/Images/FullMoon.jpg'} ),
      maintainAspect : true
    },
    
    // We can add different overlays on top of the background image.
    overlays : {
      
      sigma : 2.0,
      
      // Render layers ordered front-to-back
      // *** ADD YOUR OWN OVERLAYS HERE ***
      layers : [
    
        {
          image : Game.Graphics.Sprite({ imageURL : 'Assets/Images/Clouds2.png' }),
          mode : RenderLevelImageMode.Normal, // default
          sigmaOverride : 0.35
          //periodicity : 2 
        },
        
        {
          image : Game.Graphics.Sprite({ imageURL : 'Assets/Images/SpookyCreatures01.png' }),
          mode : RenderLevelImageMode.Normal,
          offset : { x : -400, y : -200 },
          sigmaOverride : 0.5
          //periodicity : 1
        }
    
      ]
    },
    
    // The level is made up of tiles - this tells the game engine tiles are 32 x 32 pixels in size
    // *** WHEN YOU CREATE YOUR OWN TILE SPRITES MAKE SURE THEY'RE THE SAME SIZE!!! ***
    tileWidth : 32,
    tileHeight : 32,
    
    // The level itself is an array (or collection) or tiles.  Here we're saying we want a level that's 32 tiles across and 12 tiles vertically.
    mapDimension : { width : 32, height : 12 },
    
    // mapTileArray is the actual implementation of the level design.  Each number in the array represents a different object...
    // 0 = empty space
    // 1 or 2 represent the cobblestone or grass sprites loaded above
    // -1 indicates where the player will start
    // 10, 11 and 12 represent the pickup types (see game_objects.js for the pickup types and their id values)
    // *** DESIGN YOUR LEVEL AROUND A TILE ARRAY.  EACH TILE WILL HAVE A UNIQUE ID AS ABOVE.  WHERE YOU WANT THE TILE TO APPEAR IN THE LEVEL ADD THE ID TO THE ARRAY ***
    mapTileArray : (function() {
    
      let mapImage = [
      
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0],
      [1, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
      [1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
      [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
      [1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0],
      [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 11, 0, 0],
      [2, 1, 2, 1, 2, 1, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]
      
      ];
      
      
      // Procedurally generate tile map data - stored in row major format
      /*
      let mapImage = [];
      
      for (let i=0; i<12; ++i) { // Map height iteration
        
        mapImage[i] = [];
        
        for (let j=0; j<32; ++j) { // Map width iteration
          
          // Kernel
          
          // 0 indicates an empty map tile
          mapImage[i][j] = (Math.random() < 0.1) ? 1 : 0;
        }
      }
      
      // Add floor
      for (let j=0; j<32; ++j) {
      
        mapImage[11][j] = 2;
      }    
      */
      
      return mapImage;
      
    })(),
    
    // *** MAIN PLAYER SETUP ***
    players : {
      
      factory : Game.Model.Player,
      
      model : {
        
        anim_id : 'player',
        size : { width : 32, height : 32 },
        colliderType : 'player',
        collisionFilter : Game.Model.CollisionFilter.Player,
        initState : 'Standing',
        boundingVolumeScale : { x : 0.95, y : 0.95 }
      }
    },
    
    // The dynamicElements array contains NPCs and moving platforms
    // *** ADD YOUR OWN DYNAMIC ELEMENTS BASED ON YOUR LEVEL DESIGN.  HERE YOU SPECIFY THE COORDINATES OF WHERE YOU WANT THE ITEMS TO GO - SINCE THEY MOVE AROUND THEY'RE NOT TIED TO THE TILE ARRAY! ***
    dynamicElements : [
    
    { factory : Game.Model.Creature, // Type determines actual character / behaviour
      model : {
        anim_id : 'mummy', // id determines animation sequence
        pos : { x : 64, y : 192},
        size : { width : 64, height : 64 },
        colliderType : 'mummy',
        collisionFilter : Game.Model.CollisionFilter.NPC,
        initState : 'Alive'
      } },
    
    { factory : Game.Model.Creature,
      model : {
        anim_id : 'mummy',
        pos : { x : 540, y : 336},
        size : { width : 32, height : 32 }, 
        colliderType : 'mummy',
        collisionFilter : Game.Model.CollisionFilter.NPC,
        initState : 'Alive'
      } },
      
    { factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 0, y : 0},
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        collisionFilter : Game.Model.CollisionFilter.Platform,
        initState : 'Default',
        delegate : Game.Delegates.Rotator,
        delegate_model : {
        
          origin : { x : 200, y : 100 },
          radius : 128,
          theta : 0, // determines where the platform starts
          speed : 90, // theta delta per second
          thetaDirection : -1 // determines the change of theta
        }
      } },
    
    { factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 0, y : 0},
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        collisionFilter : Game.Model.CollisionFilter.Platform,
        initState : 'Default',
        delegate : Game.Delegates.Cycler,
        delegate_model : {
        
          origin : { x : 350, y : 0 },
          direction : { x : 0, y : 280 },
          speed : 0.25,
          t : 0.5,
          tDirection : -1
        }
      } }
      
    
      
    
      
    /*{ factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 0, y : 0},
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        initState : 'Rotator',
        state_config : {
        
          origin : { x : 200, y : 100 },
          radius : 128,
          initTheta : 0, // determines where the platform starts
          speed : 90, // theta delta per second
          direction : -1 // determines the change of theta
        }
      } }
      
    { factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 0, y : 0},
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        initState : 'Rotator',
        state_config : {
        
          origin : { x : 200, y : 100 },
          radius : 128,
          initTheta : 180, // determines where the platform starts
          speed : 90, // theta delta per second
          direction : -1 // determines the change of theta
        }
      } }*/
      
   /* { factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 380, y : 130},
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        initState : 'Oscillator',
        state_config : {
        
          range : { x : 50, y : 0 },
          speed : 180, // theta delta per second
          initTheta : 0, // determines where in the range the platform starts
          direction : -1 // determines the change of theta
        }
      } }
    
    { factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 210, y : 130},
        size : { width : 64, height : 16 },
        colliderType : 'platform',
        initState : 'Oscillator',
        state_config : {
        
          range : { x : 50, y : 0 },
          speed : 180, // theta delta per second
          initTheta : 0, // determines where in the range the platform starts
          direction : 1 // determines the change of theta
        }
      } }*/
      
    /*{ factory : Game.Model.Platform,
      model : {
        anim_id : 'block_platform',
        pos : { x : 300, y : 130},
        size : { width : 32, height : 16 },
        colliderType : 'platform',
        initState : 'Cycle',
        state_config : {
        
          type : 'horizontal',
          speed : 16,
          direction : 'right',
          range : { min : 50, max : 300 }
        }
      } }*/
    
    ]
    
  };
  
  
  return module;
  
})((Game.Levels || {}));