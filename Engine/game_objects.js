'use strict';

// Render level modes - these control how your background images are rendered in relation to the main webpage canvas and camera viewport size.

let RenderLevelImageMode = Object.freeze({
  "Normal"    : 1,
  "Tile"      : 2,
  "Fill"      : 3   // Fit to world coordinates
});


// This module contains the behaviours for the moving platforms
Game.Delegates = (function(module) {
  
  // The oscillator platform moves up / down or from size-to-side.
  module.Oscillator = function(model) {
    
    let delegate = {};
    
    let ps = {
      
      origin : { x : model.origin.x, y : model.origin.y },
      
      // Unit-length direction vector for the oscillation movement
      direction : { x : model.direction.x, y : model.direction.y },
      
      range : model.range,
      speed : model.speed, // theta delta per second
      
      theta : (model.theta || 0), // optional - determines where in the range the platform starts    
      thetaDirection : (model.thetaDirection || 1) // optional - determines the change of theta
    };
    
    delegate.update = function(host, env, tDelta) {
      
      ps.theta += ps.speed * tDelta * ps.thetaDirection;
      
      // Ensure sign lies in [0, 360) interval.
      ps.theta = ps.theta % 360;
      
      if (ps.theta < 0) {
        
        ps.theta += 360;
      }
      
      // Calculate and return new position
      let x = Math.sin(Game.Math.Radian * ps.theta) * ps.range * ps.direction.x + ps.origin.x;
      let y = Math.sin(Game.Math.Radian * ps.theta) * ps.range * ps.direction.y + ps.origin.y;
      
      return { position : { x : x, y : y } };
    }
    
    return delegate;
  };
  
  // The cycler platform moves in a single direction and jump back to the start when it reaches the end of its range.
  module.Cycler = function(model) {
  
    let delegate = {};
    
    let ps = {
      
      origin : { x : model.origin.x, y : model.origin.y },
      direction : { x : model.direction.x, y : model.direction.y },
      
      speed : model.speed,
      t : (model.t || 0),
      tDirection : (model.tDirection || 1)
    };
    
    delegate.update = function(host, env, tDelta) {
      
      ps.t += ps.speed * tDelta * ps.tDirection;
      
      if (ps.t > 1.0) {
        
        ps.t -= 1.0;
      }
      else if (ps.t < 0.0) {
        
        ps.t += 1.0;
      }
      
      let x = ps.origin.x + ps.direction.x * ps.t;
      let y = ps.origin.y + ps.direction.y * ps.t;
      
      return { position : { x : x, y : y } };
    }
    
    return delegate;
  }
  
  // The rotator platform moves in a circular motion
  module.Rotator = function(model) {
    
    let delegate = {};
    
    // Setup private state and initialise
    let ps = {
      
      origin : { x : model.origin.x, y : model.origin.y },
      radius : model.radius,
      speed : model.speed,
      theta : (model.theta || 0),
      thetaDirection : (model.thetaDirection || 1)
    };
    
    delegate.update = function(host, env, tDelta) {
      
      ps.theta += ps.speed * tDelta * ps.thetaDirection;
      
      // Ensure sign lies in [0, 360) interval.
      ps.theta = ps.theta % 360;
      
      if (ps.theta < 0) {
        
        ps.theta += 360;
      }
      
      // Calculate and return new position
      let x = Math.cos(Game.Math.Radian * ps.theta) * ps.radius + ps.origin.x;
      let y = Math.sin(Game.Math.Radian * ps.theta) * ps.radius + ps.origin.y;
        
      return { position : { x : x, y : y } };
    }
    
    return delegate;
  };
  
  return module;
  
})((Game.Delegates || {}));


// The Pickup types module defines the types of pickups you find.  Types are given a unique id just like sprites above.  So the points pickup has an id of 10 for example.
Game.PickupTypes = (function(types) {
  
  types[10] = Game.Model.Pickup.Type( {
    
    // Types MUST have certain properties set...
    
    // spriteURI specifies the sprite image file to show
    spriteURI : 'Assets//Images//pickup_points.png',
    
    // The handler function determines what happens when a collector (the player for example) pickups up the pickup
    handler : function(collector) {
    
      // In this example we execute the function addPoints on the collector to given them 50 points.
      collector.addPoints(50);
    }
    
  });
  
  // Health pickup
  types[11] = Game.Model.Pickup.Type( {
    
    spriteURI : 'Assets//Images//pickup_energy.png',
    handler : function(collector) {
    
      collector.addStrength(20);
    }
  });

  // Extra life pickup
  types[12] = Game.Model.Pickup.Type( {
    
    spriteURI : 'Assets//Images//pickup_extralife.png',
    handler : function(collector) {
    
      collector.addExtraLife();
    }
  });
  
  return types;
  
})(Game.PickupTypes || []);


Game.ProjectileTypes = (function(types) {

  types['bullet'] = Game.Model.Projectile.Type({
    
    spriteURI : 'Assets/Images/bullet01.png',
    
    range : 300,
    
    bodyProperties : {
      
      isStatic : true,
      isSensor : true
    },
    
    states : {
        
      'Existing' : function(params = {}) {
        
        let state = Game.Model.State(params);
        
        let state_ps = {
          
          direction : params.initModel.direction
        };
        
        state.update = function(hostEntity, env, tDelta) {
                    
          let body = hostEntity.body();
          Matter.Body.setPosition(body, {x : body.position.x + 100 * state_ps.direction * tDelta, y : body.position.y});
          
          return true;
        }
        
        return state;        
      },
      
      'End' : function(params = {}) {
        
        let state = Game.Model.State(params);
      
        state.enter = function(hostEntity, env, tDelta) {
          
          console.log('bullet End!');
        }
        
        state.update = function(hostEntity, env, tDelta) {
          
          console.log('bullet done!');
          return false;
        }
        
        return state;
      }
    }
    
  });
  
  
  types['grenade'] = Game.Model.Projectile.Type({
    
    spriteURI : 'Assets/Images/bullet01.png',
    
    range : 100,
    
    bodyProperties : {
      
      mass : 5.0,
      inverseMass : 1.0 / 5.0,
      isSensor : true,
      frictionAir : 0
    },
    
    states : {
        
      'Existing' : function(params = {}) {
        
        let state = Game.Model.State(params);
        
        // Capture relevant config info from params here
        let state_ps = {
          
          initPos : params.initModel.pos,
          initForce : params.initModel.force
        };
        
        state.enter = function(hostEntity, env, tDelta) {
          
          Matter.Body.applyForce(hostEntity.body(), state_ps.initPos, state_ps.initForce);
        }
        
        return state; // Simple 'do nothing' state        
      },
      
      'End' : function(params = {}) {
        
        let state = Game.Model.State(params);
      
        state.update = function(hostEntity, env, tDelta) {
          
          console.log('grenade done!');
          return false;
        }
        
        return state;
      }
    }
    
  });
  
  return types;
  
})(Game.ProjectileTypes || []);
