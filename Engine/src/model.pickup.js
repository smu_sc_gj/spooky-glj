'use strict';

// Pickups

Game.Model = (function(module) {
  
  module.Pickup = (module.Pickup || {});
  
  module.Pickup.Type = function(model) {
    
    let entity = {};
    
    let ps = {
      
      handler : model.handler || function(collector) {},
      colliderType : model.colliderType || 'pickup',
      collisionFilter : model.collisionFilter || Game.Model.CollisionFilter.Pickup,
      sprite : Game.Graphics.Sprite({imageURL : model.spriteURI}),
      
      // Setup state generator functions - NOT an actual graph - this is per-instance since each 'state' can have per-instance attributes.
      states : model.states || {
        
        'Existing' : function(params = {}) {
          
          let state = Game.Model.State(params); // Simple 'do nothing' state
          
          return state;        
        },
        
        'End' : function(params = {}) {
          
          let state = Game.Model.State(params);
        
          state.update = function(hostEntity, env, tDelta) {
                  
            return false;
          }
          
          return state;
        }
      },
      
      transitions : model.transitions || [
      
      { source : 'Existing',
        target : 'End',
        cond : function(hostEntity, env, tDelta) {
          
          return (hostEntity.collected() == true);
        }
      }]
      
    };
    
    //{ Private API
    
    let handler = function() {
      
      return ps.handler;
    }
    
    let colliderType = function() {
      
      return ps.colliderType;
    }
    
    let collisionFilter = function() {
      
      return ps.collisionFilter;
    }
    
    let sprite = function() { 
    
      return ps.sprite;
    }
    
    let states = function() {
      
      return ps.states;
    }
    
    let transitions = function() {
      
      return ps.transitions;
    }
    
    //}
    
    
    //{ Public interface
    
    entity.handler = handler;
    entity.colliderType = colliderType;
    entity.collisionFilter = collisionFilter;
    entity.sprite = sprite;
    entity.states = states;
    entity.transitions = transitions;
    
    //}
    
    
    return entity;
  }
  
  
  module.Pickup.Instance = function(model, world) {
    
    let entity = Game.Model.Entity(model);
    
    //{ Private state
    
    let scale = model.scale || Game.config.pickup_sprite_scale;
    let w = model.type.sprite().image().width * scale * model.boundingVolumeScale;
    let h = model.type.sprite().image().height * scale * model.boundingVolumeScale;
        
    let ps = {
      
      body : Matter.Bodies.rectangle(model.pos.x, model.pos.y, w, h,
                              {
                                  isStatic : model.isStatic,
                                  isSensor : true,                                  
                                  collisionFilter : model.type.collisionFilter()
                              }),
      type : model.type,
      scale : scale,
      size : { width : w, height : h },
      collected : false
    };
    
    ps.body.hostObject = entity;
    Matter.World.add(world, ps.body);
    
    //}
    
    
    //{ State graph setup
        
    let states = ps.type.states();
    for (var key in states) {
      
      if (states.hasOwnProperty(key) && states[key] !== null) {
        
        entity.addState(key, states[key]());
      }
    }
    
    // Add transitions / arcs between states
    let transitions = ps.type.transitions();
    for (let t of transitions) {
      
      entity.addTransition(t.source, t.target, t.cond);
    }
        
    //}
    
    
    //{ Private API
    
    let draw = function(context) {
    
      if (ps.body) {
        
        context.save();
        
        var pos = ps.body.position;
        
        context.translate(pos.x, pos.y);
        context.translate(-ps.type.sprite().image().width * ps.scale / 2, -ps.type.sprite().image().height * ps.scale / 2);
        ps.type.sprite().draw(context, { x : 0, y : 0, scale : ps.scale });
        
        context.restore();
        
        if (Game.config.show_bounding_volume) {

          
          var vertices = ps.body.vertices;
          
          context.beginPath();
          
          context.moveTo(vertices[0].x, vertices[0].y);
          
          for (var j = 1; j < vertices.length; ++j) {
          
            context.lineTo(vertices[j].x, vertices[j].y);
          }
          
          context.lineTo(vertices[0].x, vertices[0].y);
              
          // Render geometry
          context.lineWidth = 1;
          context.strokeStyle = '#FFFFFF';
          context.stroke();          
        }
      }
    }
      
    
    // Accessor methods
    
    let body = function() {
      
      return ps.body;
    }
    
    let type = function() {
      
      return ps.type;
    }
    
    // Interface used by transition lambdas
    let collected = function() {
      
      return ps.collected;
    }
    
    let setCollected = function() {
      
      ps.collected = true;
    }
    
    //}
    
    
    //{ Public interface
    
    entity.draw = draw;
    entity.body = body;
    entity.type = type;
    entity.collected = collected;
    entity.setCollected = setCollected;
    entity.CollisionInterface = Game.Model.CollisionInterface(ps.type.colliderType());
    
    //}
    
    // Inital state entry
    entity.setCurrentState(model.initialState || 'Existing', model.env, model.tDelta);
    
    return entity;
  }  
  
  return module;
  
})((Game.Model || {}));