'use strict';

// Math Library - avoid Math namespace pollution

window.Game = (function(module) {
  
  module.Math = ( module.Math || {} );
  
  module.Math = {
    
    Radian : Math.PI / 180.0
  } 
  
  return module;
  
})((window.Game || {}));
