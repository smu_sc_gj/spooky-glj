
// Model projectile type

Game.Model = (function(module) {
  
  module.Projectile = module.Projectile || {};
  
  module.Projectile.Type = function(model = {}) {
    
    let entity = {};
    
    let ps = {
      
      strength : model.strength || 10,
      range : model.range || 200,
      //mass : model.mass || 5.0,
      rechargeTime : model.rechargeTime || 0.1, // Move to "weapon" class that actually fires??
      
      // colliderType is engine level => determines collision handlers called
      colliderType : model.colliderType || 'projectile',
      
      // matter.js collision filters - leave for instance so we can filter per-projectile
      // what it hits (rather than type).  So players and NPCs can fire same type but collision
      // managed per-instance.  Otherwise need seperate type for each 'object' that fires
      // the given type.
      // collisionGroup : model.collisionGroup || 0,
      // collisionFilter : model.collisionFilter || Game.Model.CollisionFilter.Projectile,
      
      // default sprite???
      sprite : model.spriteURI ? Game.Graphics.Sprite({imageURL: model.spriteURI}) : null,
    
      // Setup state generator functions - NOT an actual graph - this is per-instance since each 'state' can have per-instance attributes.
      states : model.states || {
        
        'Existing' : function(params = {}) {
          
          let state = Game.Model.State(params);
          
          return state; // Simple 'do nothing' state        
        },
        
        'End' : function(params = {}) {
          
          let state = Game.Model.State(params);
        
          state.update = function(hostEntity, env, tDelta) {
                  
            return false;
          }
          
          return state;
        }
      },
      
      transitions : model.transitions || [
      
      { source : 'Existing',
        target : 'End',
        cond : function(hostEntity, env, tDelta) {
          
          return hostEntity.distanceTravelled() >= hostEntity.type().range();
        }
      }],

      bodyProperties : model.bodyProperties || {}
    };
    
    
    //{ Public interface
    
    
    // Accessor methods
    
    entity.strength = function() {
      
      return ps.strength;
    }
    
    entity.range = function() {
      
      return ps.range;
    }
    
    /*entity.mass = function() { 
    
      return ps.bodyProperties.mass;
    }*/
    
    entity.rechargeTime = function() {
      
      return ps.rechargeTime;
    }
    
    entity.colliderType = function() {
      
      return ps.colliderType;
    }
    
    /*entity.collisionFilter = function() {
      
      return ps.collisionFilter;
    }*/
    
    entity.sprite = function() {
      
      return ps.sprite;
    }
    
    entity.states = function() {
      
      return ps.states;
    }
    
    entity.transitions = function() {
      
      return ps.transitions;
    }
    
    entity.bodyProperties = function() {
      
      return ps.bodyProperties;
    }
    
    //}
    
    return entity;
  }
  
  
  module.Projectile.Instance = function(model, world) {
    
    let entity = Game.Model.Entity(model);
    
    //{ Private state
    
    let scale = model.scale || Game.config.projectile_default_sprite_scale;
    let boundingVolumeScale = model.boundingVolumeScale || 0.8;
    let radius = model.type.sprite().image().width * scale * boundingVolumeScale;
    
    // Use assign to construct a bodyProperties object for matter.js body creation.
    let bodyProperties = Object.assign(model.type.bodyProperties(), model.collisionFilter);
    
    let ps = {
      
      type : model.type,
      owner : model.owner, // Entity that fired projectile
      
      scale : scale,
      
      // For now fixed bounding volume - allow this to be configured!
      // Note: As noted above, collisionFilter is not derived from the projectile type so the same type can have different collision profiles based on who fired it
      body : Matter.Bodies.circle(model.pos.x, model.pos.y, radius, bodyProperties),
      
      oldPos : { x : model.pos.x, y : model.pos.y },
      distanceTravelled : 0 // distance travelled controlling lifespan of projectile (distanceTravellent <= model.type.range())
    };
    
    ps.body.hostObject = entity;
    Matter.World.add(world, ps.body);
    
    //}
    
    //{ State graph setup
    
    let states = ps.type.states();
    for (var key in states) {
      
      if (states.hasOwnProperty(key) && states[key] !== null) {
        
        entity.addState(key, states[key]({ initModel : model }));
      }
    }
    
    // Add transitions / arcs between states
    let transitions = ps.type.transitions();
    for (let t of transitions) {
      
      entity.addTransition(t.source, t.target, t.cond);
    }
    
    //}
    
    
    //{ Private API
    
    // Override default update to perform range tracking.  Could be done in actual state update handlers but handle here since all projectiles track this.
    entity.update = function(env, tDelta) {
            
      let status = entity.currentState().update(this, env, tDelta);
      
      // Update distance travelled
      let dx = ps.body.position.x - ps.oldPos.x;
      let dy = ps.body.position.y - ps.oldPos.y;
      
      ps.distanceTravelled += Math.sqrt(dx*dx + dy*dy);
      
      ps.oldPos.x = ps.body.position.x;
      ps.oldPos.y = ps.body.position.y;
      
      return status;
    }
    
    let draw = function(context) {
    
      if (ps.body) {
        
        context.save();
        
        var pos = ps.body.position;
        
        context.translate(pos.x, pos.y);
        context.translate(-ps.type.sprite().image().width * ps.scale / 2, -ps.type.sprite().image().height * ps.scale / 2);
        ps.type.sprite().draw(context, { x : 0, y : 0, scale : ps.scale });
        
        context.restore();
        
        if (Game.config.show_bounding_volume) {

          
          var vertices = ps.body.vertices;
          
          context.beginPath();
          
          context.moveTo(vertices[0].x, vertices[0].y);
          
          for (var j = 1; j < vertices.length; ++j) {
          
            context.lineTo(vertices[j].x, vertices[j].y);
          }
          
          context.lineTo(vertices[0].x, vertices[0].y);
              
          // Render geometry
          context.lineWidth = 1;
          context.strokeStyle = '#FFFFFF';
          context.stroke();          
        }
      }
    }
    
    // Accessor methods
    
    let type = function() {
      
      return ps.type;
    }
    
    let body = function() {
      
      return ps.body;
    }
    
    let position = function() {
      
      return ps.body.position;
    }
    
    let setPosition = function(pos) {
      
      ps.body.position = { x : pos.x, y : pos.y };
    }
    
    let distanceTravelled = function() {
      
      return ps.distanceTravelled;
    }
    
    //}
    
    
    //{ Public interface
    
    entity.draw = draw;
    entity.type = type;
    entity.body = body;
    entity.position = position;
    entity.setPosition = setPosition;
    entity.distanceTravelled = distanceTravelled;
    
    //}
    
    
    // Initialise current state
    entity.setCurrentState(model.initialState || 'Existing', model.env, model.tDelta);
    
    
    return entity;
  }
  
  
  return module;
  
})(Game.Model || {});
