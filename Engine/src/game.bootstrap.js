'use strict';

// Main game bootstrap

window.Game = (function(module) {
  
  module.bootstrap = function(params) {
    
    // Setup plug-ins for physics engine
    Matter.use('matter-attractors');
  
    Game.canvas = $('#canvas')[0];
    Game.context = Game.canvas.getContext('2d');
    Game.system = Game.System();
    
    // Create and run game stage graph        
    let mainGame = Game.Stages.MainGameStage();
    
    Game.stageGraph = mainGame;
    Game.stageGraph.init( { level : 1 } );
  } 
  
  return module;
  
})((window.Game || {}));
