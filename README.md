# README #

# Introduction #



## Copying ##

See [licence](Licence.md)

## Development Notes ##

~~RAMBLINGS~~

### Manage contact profile interface and responsibility ###

Sometimes character does NOT lose binding - perhaps contact distribtion changes and so resulting profile bit changes too.  Net result is we lose contact but don't lose binding.


Contact modelling -

An object can maintain multiple object contacts

Each pair contact has supports - we can derive stats from this.
As objects move, so supports + stats change
Each pair has a 'profile' - bit flags set to indicate edge in contact
(note: could be multiple edges, but we decide on a single edge for now!!!)

Currently, we store a single set of 4 bits for each edge
But ideally we change this!

For an object that maintains a contact profile...

Want to store contact between two objects -> how can we address this?
Matter gives us a unique id for collision - ideally use this!

pair.id should be okay for this -> it's an ordered combination on the object ids - so consistent no matter the order the objects are paired in.

Use this in Map to determine object.

On collision start, setup
On collision active, update
On collision end, delete from Map



## TODO List ##


1) When we delete an object (like a pickup) what happens to existing collisions?
Resolved : Yes - Matter.js handles this!


2) ~~A pickup may not be allowed - depending on conditions.  The pickup knows what those conditions are, so expects the 'collector' to respond to certain queries in order to know if collection is possible (the collector does not implement this logic, otherwise the collector would need to implement logic for EVERY pickup!!  Not ideal!)~~

Resolved:
So, add handler() to pickup - formalise idea that pickup affects related group of collector's properties - so provide interface for said properties that pickup talks to.

*** 2.1) Add validator() to pickup that takes collector and queries same interface to see if it can in fact be picked up (the validator returns true if it can, false if it cannot).


3) We maintain a contact model to facilitate state change.  We could change state on a collision start / end, but multiple factors might be the basis of a state change, so we maintain a contact profile to provide this information along with other state information needed for character state transitions.

The problem with the following model...

beforeUpdate - clear contact profiles for host objects
collision start - build Matter collisions
collision active - build host object contacts from matter collisions
collision end

Now, if we build contact profile with active collisions, this does not include new collisions in the current frame, so inconsistency exists.

We could modify profile on start and end - leaving active not performing any operation on the contact profile.  We also do not clear profiles on beforeUpdate.

However, on active collision we need to reevaluate contacts - without this we assume contacts don't change between a start and end collision for any two given objects.


So,
Start of frame, object exist in current state based on current contact + state in game and matter.js.  'beforeupdate' handles character (+ other game) updates based on current state

matter updates + calls events in given order (collision start, active, end)

AT THIS POINT WE HAVE (POTENTIALLY) NEW CONTACT STATE IN MATTER.JS

In 'afterupdate' we manage transitions to ensure consistency of character + in-game object state with matter.js (contact) state


So to ensure consistency we do the following...
collision start: Add new contact profile to object
collision active: Recalc and update contact profile if necessary (contacts may change while a collision remains active)
collision end: Remove contact profile from object

To manage this, each object maintains a contact count profile - an ordered 4 int array for <#bottom, #top, #left, #right> values.

For add an remove only need count for whole object.  But for active, where we track contact changes, we need to know per-object pair contacts, so Map becomes relevant here!


Resolution: Don't do this!  Reset contacts at start of frame, update on start + active (which doesn't include contacts resulting from end collision events) and then update states based on this.


------------------------------------


4) Contact profile: Small contacts result in incorrect left / right contact profile bits being set.  Want to recalculate profile using thesholding of support vertices!
Resolved - RLE bounding volume of left / right adjacent static tiles

*** 4.1) ~~RLE of horizontal static tile sets~~
Compelted

*** 4.2) Recalcuate contact profile - Rework contact profile calculation to factor in matter.js slop coefficient and avoid false side contacts.


------------------------------------

5.1) Do we really need game level object id (body id used for collisions etc - check this!)
Resolved - yes - serves as id for spritesheet and animation sequence associations

5.2) id of player Matter.js body object seems to alter behaviour -
Resolved - string-based id used for animation association - let Matter.js use own internal numeric id assignment

5.3) Rename generic 'id' at game model level to animation_id to more clearly identify role of field
Completed


------------------------------------


6) ~~Pickups just deleted in collision handler - does not take into account animation etc., so remove this, tie to stat-[=e model and on end state update return false.  Let main beforeUpdate handler then handle removal as with other state-based objects
Notes:  Will require we have a 'collected' flag to trigger a transition~~
Completed


------------------------------------


7) ~~beforeUpdate is responsible for calling update on ALL matter.js bodies.  We still waste time iterating over static objects with no update.  Replace this to update player + dynamic elements~~
Completed


------------------------------------


8) Types vs. Level config

Class model defines the core states and behaviour of an in-game character, or element.  The animation / spritesheet models the appearance (skin) of the character.  Pickups delegate to the type for the pickup handler, but not for the actual updates.

To delegate updates, we'd need to selectively delegate based on state.  The main update method calls the state update method.

Should we allow delegation from state update methods?

Current model uses subclassing and overriding, so in defining the default state behaviour we need to explicitly manage delegation.
Not ideal!
Better options?
i) Create new state class and allow this to replace existing state for a given entity instance
ii) Register state methods with base entity and in turn register an alternative.
For (ii) do we replace the previous state handler or provide option to fall back on default?
In delegating to create alternative behaviour, we may need new state in the delegate to handle tasks.  We therefore end up with what is close to a new state object.


------------------------------------



11) ~~Add ability to provide a state transition from 'any' state - so we don't have to specify transition for every state explicity.
Do this by passing null as the 'current' state in the addTransition method.  Then add the transition object to ALL existing states.  THIS REQUIRES ALL STATES TO BE SETUP BEFORE TRANSITIONS?~~
Resolved: Yes - Game engine requires states setup before transitions.  A tool can manage this separately if flexibility needed.
Completed


------------------------------------


12) ~~Game object update functions recieve time delta in actual seconds, not milliseconds.~~
Completed


------------------------------------


13) ~~Sort rotator platform type (extend by adding states)~~
Completed - Added delegation model.  All behaviour-specific state stored in the delegate.


------------------------------------


14) Level editor



15) Expand NPC types



16) B-Spline based platform paths
Added basic b-spline model.  To add:
- Since curve types determined by coefficient matrix in current implementation, class model seems too cumbersome - consider JS prototyping only
- Add derivative basis function option to calc tangents (for 2D engine normals perp-dot-product)


------------------------------------

17) ~~Ensure all objects have correct collision Category + Mask attributes set~~
Completed

------------------------------------


19) Add Perlin noise to Math


20) Add projectiles

To sort...
Type provides a consistent interface, but what of data specific to types and not shared?  Subclassing model suitable here or prototyping.  eg. bullet has speed while grenade has initial force.

Introduce weapon type that has own update with key press - so fire NOT in player!  much better since we don't duplicate logic between player states for example.


21) Add particle system

Consider particles acting in 2 ways...

Type 1 - Interactive - participate in collisions + physics environment
Type 2 - Non-interactive - visual effect (background for example).  Does not interaction - no collisions - but subject to physics behaviour

For type 2, we can afford larger particle sets.  Don't use matter.js, but apply relevant physics.

For type 1, can be scalable, but integrated into matter.js.  Update in two parts - matter.js update followed by particle age update managed by main game engine.  Projectiles fit into type 1 particle model (if we merged the systems).

Type 1 particles can be modelled using one of two approaches:
i) JavaScript objects (as is the case with Explorer01c projectiles)
ii) Pre-calculated matter.js objects where non-active particles are put to 'sleep'.  Need to check if this impacts performance of engine!?  These contain position data etc.  Don't have cache (spatial) locality here - matter.js objects similar to (i) above.  Only engine-level particle model would utilise type 2 packed array model.  Not sure if any benefit can be obtained using this approach.  *** NO - SLEEPING BODIES DO NOT FIT THIS REQUIREMENT - see https://github.com/liabru/matter-js/issues/354)

For type 2 we create packed typedarray based models of the particles.  We don't use matter.js to update, but employ own physics model to manipulate.




22) Expand binding system introduced for model.platform to allow objects to be "stuck" together and perhaps introduce a messaging system between bound objects.


23) State-based properties: When crouching for example may need to vary bounding box for example.
On entry into a state we establish the properties needed - however, need to be mindful of constraints - can be actually adjust properties, or do we need to modify level design for example to accomodate such changes.  For example: we crouch, then fall of ledge.  What is ceiling to still low - going to a full height sprite will cause intersection with the ceiling.???!!!!



24) Refactor mass and inverseMass to use density instead?

25) Refactor to more clearly show dynamic and kinematic entity subtypes and have clear update policy
